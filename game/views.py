from datetime import timedelta
from threading import Timer
from django import forms
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.core.cache import cache, get_cache
import json
from django.core.urlresolvers import reverse
from django.db.models.aggregates import Count, Sum
from django.db.models.expressions import When
from django.db.models.fields import IntegerField
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.utils import timezone
from django.utils.html import strip_tags
from django.utils.translation import ugettext as _
# from django.utils.translation import ugettext_lazy as _

# Create your views here.
from django.views.generic.base import TemplateResponseMixin, View, TemplateView
from django.views.generic.edit import FormView
from game.models import Lobby, Gamer
import logging
logger = logging.getLogger(__name__)

redis = cache.client.get_client(True)


class LobbyForm(forms.ModelForm):
    class Meta:
        model = Lobby
        fields = ['name', 'gamers_count']


def doesnt_allow_join(request):
    return request.user.gamer_set.filter(lobby__end_time__isnull=True,
                                         left_time__gte=timezone.now()-timedelta(minutes=1)
                                         ).exists()


class MainView(FormView):
    template_name = 'game/main.html'
    form_class = LobbyForm

    def __init__(self):
        super(MainView, self).__init__()
        self.lobby = None

    def get_success_url(self):
        if self.lobby:
            return reverse('lobby', kwargs={'lobby_id': self.lobby.id})
        else:
            return reverse('main')

    def form_valid(self, form):
        if not self.request.user.is_authenticated():
            return super(MainView, self).form_valid(form)

        if doesnt_allow_join(self.request):
            # Yes, yes. I know: I there is messages django API.
            self.request.session['flash'] = _("You can join other lobby only one minute after you left lobby")
            return super(MainView, self).form_valid(form)
        else:
            lobby = form.save(commit=False)
            lobby.name = strip_tags(lobby.name)
            lobby.save()
            # lobby.gamer_set.create(user=request.user, is_creator=True)
            # послать добавление лобби
            redis.publish('/ws/list', json.dumps(dict(
                action='add',
                id=lobby.id,
                name=strip_tags(lobby.name),
                count=lobby.gamers_count,
                cur_cnt=1,
                status=lobby.get_status(),
                url=reverse('lobby', kwargs={'lobby_id': lobby.id})
            )))
            self.lobby = lobby
            return super(MainView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        lists = {2: [], 4: []}
        users_lobbies = set()
        if self.request.user and self.request.user.is_authenticated():
            self.request.user.gamer_set\
                .filter(
                    left_time__isnull=True,
                    lobby__start_time__isnull=False
                )\
                .update(left_time=timezone.now())  # ban it left while game was running

            for gamer in self.request.user.gamer_set.filter(join_time__isnull=False):
                redis.publish('/ws/list', json.dumps(dict(
                    action='update',
                    id=gamer.lobby_id,
                    cur_cnt=gamer.lobby.gamer_set
                    .filter(join_time__isnull=False)
                    .exclude(user=self.request.user).count(),
                    status=gamer.lobby.get_status(),
                )))

            self.request.user.gamer_set.all()\
                .update(join_time=None)

            users_lobbies = {
                l.lobby_id for l in self.request.user
                .gamer_set
                .filter(join_time__isnull=False)
                .only('lobby_id')
            }
            lobbies = Lobby.objects\
                .filter(gamer__join_time__isnull=False)\
                .annotate(cur_cnt=Count('gamer'))

            for lobby in lobbies.order_by('create_time'):
                lists[lobby.gamers_count].append(lobby)

            for lobby in Lobby.objects.exclude(pk__in=lobbies):
                lists[lobby.gamers_count].append(lobby)

        context.update({
            'login_form': AuthenticationForm(),
            'lists': lists,
            'users_lobbies': users_lobbies,
            'time_to_allow_join': 60,
            'STATUSES': json.dumps({k: str(v) for k, v in Lobby.STATUSES.items()}),
            'msg': self.request.session.get('flash')
        })
        if self.request.session.get('flash'):
            del self.request.session['flash']
        return context


class LobbyView(TemplateView):
    template_name = 'game/game.html'

    def get(self, request, *args, **kwargs):
        # Пользователи, покинувшие лобби в ходе игры, не могут вступать ни в какие лобби в течение минуты.
        # найти игры пользователя в которых он еще участвует
        lobby_id = kwargs.get('lobby_id')
        if doesnt_allow_join(request):
            request.session['flash'] = _("You can join other lobby only one minute after you left lobby")
            return HttpResponseRedirect(reverse('main'))
        lobby = Lobby.objects.filter(id=lobby_id).first()
        if not lobby:
            request.session['flash'] = _("No lobby exits")
            return HttpResponseRedirect(reverse('main'))

        if lobby.get_status() == 'running':
            request.session['flash'] = _("Game already running")
            return HttpResponseRedirect(reverse('main'))

        current_gamers = lobby.gamer_set.filter(join_time__isnull=False)
        current_gamers_count = current_gamers.count()
        if current_gamers_count >= lobby.gamers_count:
            request.session['flash'] = _("This lobby already full")
            return HttpResponseRedirect(reverse('main'))

        # поскольку могут выходить  и выходить, то свободные места могут быть не по порядку.

        free_positions = set(range(lobby.gamers_count)) - {g.position for g in current_gamers.only('position')}
        first_free_position = None
        if free_positions:
            first_free_position = list(free_positions)[0]
        else:
            logger.error("Something go wrong: count gamers is, but no free positions".format(current_gamers_count))

            request.session['flash'] = _("This lobby already full")
            return HttpResponseRedirect(reverse('main'))

        # если текущий пользователя нет, то добавить его в лобби,

        gamer = lobby.gamer_set.filter(user=request.user  #, left_time__isnull=True  # если разрешено возвращаться
                                       ).first()
        if not gamer:
            gamer = Gamer(
                lobby=lobby,
                user=request.user,
            )
        gamer.left_time = None
        gamer.join_time = timezone.now()
        gamer.position = first_free_position
        gamer.save()

        current_gamers_count += 1

        # Сообщить об изменении количества игроков
        redis.publish('/ws/list', json.dumps(dict(
            action='update',
            id=lobby_id,
            name=strip_tags(lobby.name),
            status=lobby.get_status(),
            cur_cnt=current_gamers_count
        )))
        #   при этом послать сообщение в канал лобби, о добавлении игрока
        redis.publish('/ws/lobby/{}'.format(lobby.id), json.dumps(dict(
            action='add',
            uid=request.user.id,
            name=strip_tags(request.user.username),
            index=gamer.position
        )))

        logger.warning("::: {}\tam_i_creator = {}".format(gamer.user, gamer.is_creator))

        page_data = {
            "lobby": lobby,
            "am_i_creator": gamer.is_creator,
        }
        page_data.update(kwargs)
        return super(LobbyView, self).get(request, *args, **page_data)

    def get_context_data(self, **kwargs):
        gamers = [Gamer(user=User(username=_("waiting")))] * kwargs['lobby'].gamers_count

        for g in kwargs['lobby'].gamer_set.filter(join_time__isnull=False).select_related('user').all():
            gamers[g.position] = g

        kwargs.update({
            'gamers': gamers
        })
        return kwargs
