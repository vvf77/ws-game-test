
lobbyTpl = $('#tpl_lobby').html();
$lobbiesContainerByCnt = {}
$('.lobbies-list-frame').each (i, frm)->
  $lobbiesContainerByCnt[$(frm).data 'gamers_count'] = $ frm
    .find '.lobbies'

$(document).on 'click', '.lobby a', ->
  $flashMsg.removeClass 'active'
  $flashMsg.removeClass 'done'
  $lnk = $ this
  window.setTimeout ->
    show_message window.MSG_JOIN_TO + ' "' + $lnk.html() + '"<br>' +
      "<a href=\"#{$lnk.attr 'href'}\">#{MSG_YES}</a>  <a href='' class='close'>#{MSG_NO}</a>"
  , 150
  false

$flashMsg = $ '.flash-msg'

show_message = (content=null)->
  $flashMsg.addClass 'active'
  if content
    $flashMsg.html content
  $closeBtn = $flashMsg.find '.close'
  if not $closeBtn.length
    $closeBtn = null
  $flashMsg.off 'click', remove_flash
  ($closeBtn or $flashMsg).on 'click', remove_flash

$(document).keydown (ev)->
  if ev.keyCode == 27 and $flashMsg.hasClass 'active'
    remove_flash()
    # here "Скобки без аргументов не нужны" - won't work
    # because it will be translated to "remove_flash;" instead of "remove_flash();"
    return false

remove_flash = ->
  $flashMsg.removeClass 'active'
  $flashMsg.addClass 'done'
  false


if $flashMsg.length

  $(document).ready ->
    window.setTimeout ->
      show_message()
      window.setTimeout remove_flash, 20000
    , 250
else
  $flashMsg = $ '<div class="flash-msg"></div>'
  $flashMsg.appendTo 'body'


# подключиться к каналу
# протокол канала:
#  add - добавить: ид, тип(2/4), название
#  update - обновить статус, и количество
#  del - удалить: ид
# Полученные данные заносить в список или обновлять их там


ws_start = ->
  ws= new WebSocket window.WS_BASE_URL + '/list'

  ws.onclose = ->
    window.setTimeout ws_start, 500
    ws = null

  ws.onmessage = (msg)->
    command = JSON.parse msg.data
    if command
      if command.action == 'add'
        newLobbysHtml = lobbyTpl
        for k, v of command
          newLobbysHtml = newLobbysHtml.replace "##{k}#", v
        $lobbiesContainerByCnt[command.count].append newLobbysHtml
        return

      if command.action == 'del'
        $ "#lobby_#{command.id}"
          .detach()
        return

      if command.action == 'update'
        $lobby = $ "#lobby_#{command.id}"
        if command.status?
          $lobby.find '.status'
            .html window.STATUSES[command.status]
        if command.cur_cnt?
          $lobby.find '.cur-cnt'
            .html command.cur_cnt
  ''

$ document
.ready ->
  window.setTimeout ws_start, 250