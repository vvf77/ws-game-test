$game = $ '.game'
lobbyId = $game.data 'lobbyid'
userId = $game.data 'userid'
messageTpl = $('#tpl_message').html()
myName = $ '.me'
  .html()   # there myName variable should be assigned result of call html, but not html which is function.
$chat = $ '#chat'
# подключиться к каналу свой игры
# протокол канала игры:
# действие:
#  add - добавление игрока. ИД, имя игрока (uid, name, index)
#  del - игрок покинул лобби: (uid)
#  start - нужное количество набрано. нет параметров
#  chat - шаг игрока. сообщение чата
#  stop - игра окончена. нет параметров
# дождаться прихода нужного количества игроков
# начать игру (чатик)
# продолжать игру

check_game_ready = ->
  if $('.gamer.waiting').length <=0 and $('.gamer.left').length <=0
    $game.removeClass 'waiting'
    $game.addClass 'ready'
    return true
  else
    $game.addClass 'waiting'
    $game.removeClass 'ready'
  false

usedWSMethods =
  send_msg = ->
    console.log 'no connection yet'

usedWSMethods.send_start = usedWSMethods.send_msg

$('#msg-form').submit ->
  usedWSMethods.send_msg $('.msg').val()
  this.reset()
  false

$startBtn = $ '.start-game-btn'
$startBtn.click ->
  usedWSMethods.send_start()
  false


ws_start = ->

  ws= new WebSocket window.WS_BASE_URL+"/lobby/#{lobbyId}/#{userId}"

  ws.onopen = ->
    console.log('open ws:', arguments)
  ws.onerror = (err)->
    console.log('error ws:', err)
    ws = null

  ws.onclose = ->
    window.setTimeout ws_start, 500
    ws = null

  ws.onmessage = (msg)->
    command = JSON.parse msg.data
    if command
      if command.action == 'stop'
        window.location = '/'
        return
      if command.action == 'start'
        $game.removeClass 'waiting'
        $game.removeClass 'ready'
        $game.addClass 'running'
        $startBtn.detach()
        return
      if command.action == 'del'
        $("#gamer_#{command.uid}").addClass 'left'
        check_game_ready()
        return

      if command.action == 'add'
        $gamer = $ ".gamer.g#{command.index}"
        $gamer.removeClass 'waiting'
        $gamer.removeClass 'left'
        $gamer.data 'uid', command.uid
        $gamer.attr 'id','gamer_'+command.uid
        $gamer.html command.name
        check_game_ready()
        return

      if command.action == 'chat'
        newMessageHtml = messageTpl
        for k, v of command
          newMessageHtml = newMessageHtml.replace "##{k}#", v
        $newMessage = $ newMessageHtml
        if command.gamer == myName
          $newMessage.addClass 'my'
        $chat.append $newMessage
        return

  usedWSMethods.send_start = ->
    if not check_game_ready()
      return
    if not ws
      window.setTimeout usedWSMethods.send_start, 500
    ws.send JSON.stringify
      action: 'start'

  usedWSMethods.send_msg = (msg)->
    if not ws or not msg
      return
    ws.send JSON.stringify
      action: 'chat'
      gamer: myName
      message: msg

  check_game_ready()  # there is call it, not return callback

$(document).ready ->
  window.setTimeout ws_start, 250