from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class Lobby(models.Model):
    class Meta:
        pass
    gamers_count_values = {
        2: _("Two gamers"),
        4: _("Four gamers")
    }
    STATUSES = {
        'done': _("Game over"),
        'waiting': _("Waiting for gamers joins"),
        'running': _("Game running"),
        'none': _("Unknown")
    }

    name = models.CharField(_("Lobby title"), max_length=128)
    gamers_count = models.IntegerField(_("Gamers count"), choices=gamers_count_values.items())
    create_time = models.DateTimeField(_("Create time"), auto_now_add=True)
    start_time = models.DateTimeField(_("Start time"), null=True, blank=True)
    end_time = models.DateTimeField(_("End time"), null=True, blank=True)
    gamers = models.ManyToManyField(User, through='Gamer')

    def get_status(self):
        if self.end_time is not None:
            return 'done'
        if self.start_time is None:
            return 'waiting'
        if self.start_time is not None:
            return 'running'
        return 'none'

    def get_status_verbose(self):
        return self.STATUSES[self.get_status()]


class Gamer(models.Model):
    user = models.ForeignKey(User)
    lobby = models.ForeignKey(Lobby)
    position = models.IntegerField(_("Position in lobby"))
    join_time = models.DateTimeField(_("Join time"), auto_now_add=True, blank=True, null=True)
    left_time = models.DateTimeField(_("Left time"), null=True, blank=True)
    result = models.CharField(_("Gamers result"), max_length=120, blank=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None and self.lobby.gamers_count <= self.lobby.gamer_set.filter(join_time__isnull=False).count():
            raise Exception('No more users allowed to this type lobby')
        if self.id is None and self.position is None:
            self.position = self.lobby.gamer_set.count()
        # сюда можно было бы из контроллера перетащить логику по поводу старта и останова игры
        super(Gamer, self).save()

    @property
    def is_creator(self):
        return self.position == 0