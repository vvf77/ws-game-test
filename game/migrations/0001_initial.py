# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Gamer',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('is_creator', models.BooleanField(verbose_name='Does this gamer create lobby?', default=False)),
                ('join_time', models.DateTimeField(auto_now_add=True, verbose_name='Join time')),
                ('left_time', models.DateTimeField(verbose_name='Left time', blank=True, null=True)),
                ('result', models.CharField(max_length=120, verbose_name='Gamers result', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Lobby',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Lobby')),
                ('gamers_count', models.IntegerField(choices=[(2, 'Two gamers'), (4, 'Four gamers')], verbose_name='Gamers count')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='Create time')),
                ('start_time', models.DateTimeField(verbose_name='Start time', blank=True, null=True)),
                ('end_time', models.DateTimeField(verbose_name='End time', blank=True, null=True)),
                ('gamers', models.ManyToManyField(through='game.Gamer', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='gamer',
            name='lobby',
            field=models.ForeignKey(to='game.Lobby'),
        ),
        migrations.AddField(
            model_name='gamer',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
