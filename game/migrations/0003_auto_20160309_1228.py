# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0002_auto_20160308_1704'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gamer',
            name='join_time',
            field=models.DateTimeField(verbose_name='Join time', auto_now_add=True, null=True),
        ),
    ]
