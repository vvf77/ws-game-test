# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gamer',
            name='is_creator',
        ),
        migrations.AddField(
            model_name='gamer',
            name='position',
            field=models.IntegerField(default=1, verbose_name='Position in lobby'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='lobby',
            name='name',
            field=models.CharField(max_length=128, verbose_name='Lobby title'),
        ),
    ]
