from django.core.management.base import BaseCommand
from optparse import make_option
from game.wsserver import run_server, logger
import logging

__author__ = 'vvf'


class Command(BaseCommand):

    help = 'Start websocket server'

    option_list = BaseCommand.option_list + (
        make_option('--port', '-p', dest='port', type="int", default=8765,
            help='listening port'),
        make_option('--host', '-H', dest='host', default='127.0.0.1',
            help='listening hostname/address'),
    )

    def handle(self, *args, **options):
        self.run(**options)

    def run(self, host='127.0.0.1', port=8765, **options):
        logger.addHandler(logging.StreamHandler())
        logger.setLevel(logging.DEBUG)
        run_server(host, port)
