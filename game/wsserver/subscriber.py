import asyncio
from django.conf import settings
# from django.core.cache import cache
import asyncio_redis
from .utils import get_params_from_url
import websockets

import logging
logger = logging.getLogger(__name__)
logger.debug('Start logger {}'.format(__name__))


redis_url = settings.CACHES['default']['LOCATION']
async_loop = asyncio.get_event_loop()


# another way is listening to pattern channel through psubscribe
class Subscriber:
    redis_connection = None
    subscriber = None
    is_running = False
    connections = {'dummy': []}
    subscriptions = ['dummy']  # ОЧЕНЬ важно чтобы это был не пустой список в начале! На выявление этой ошибку ушло 4 часа!
    redis_runner_task = None

    def __init__(self, channel, ws):
        self.channel = channel
        self.ws = ws
        self.state = 'ready'
        self.subs = None
        self.ping_handler = None
        self.plan_ping()

    def plan_ping(self):
        if self.ping_handler is not None:
            self.ping_handler.cancel()
        self.ping_handler = async_loop.create_task(self.do_ping())

    @asyncio.coroutine
    def do_ping(self):
        '''
        Пинг нужен не для того чтобы выявить оборванные соединения, а для того, чтобы они не обрывались
        :return:
        '''
        try:
            yield from asyncio.sleep(900)
            logger.debug('Ping socket ({})...'.format(self.channel))
            yield from self.ws.ping()
        except websockets.InvalidState as err:
            logger.warning('Ping says socket is closed ({})'.format(err))
            self.state = 'done'
        finally:
            self.plan_ping()

    @asyncio.coroutine
    def send(self, msg):
        if not self.ws.open:
            logger.warning('Socket closed in channel={}'.format(self.channel))
            self.state = 'done'
            return
        yield from self.ws.send(msg.value)
        self.plan_ping()

    @classmethod
    @asyncio.coroutine
    def run_redis_listener(cls):
        if cls.is_running:
            logger.debug("Already running")
            return
        cls.is_running = True
        yield from cls.get_redis_subscriber()
        logger.debug('Start from redis relay loop')
        while True:
            msg = yield from cls.subscriber.next_published()
            logger.debug('Send msg "{}" to {} subscribers'.format(msg.value, len(cls.connections[msg.channel])))
            for c in cls.connections[msg.channel]:
                if c.state != 'done':
                    yield from c.send(msg)

            if msg.channel != 'dummy':
                cls.connections[msg.channel] = [c for c in cls.connections[msg.channel] if c.state != 'done']
                if not cls.connections[msg.channel]:
                    del cls.connections[msg.channel]  # So next time after resubscribe - it won't be subscribed
                    logger.debug('delete channel {} from subscriptions'.format(msg.channel))

    @classmethod
    def start_redis_listener(cls):
        if cls.redis_runner_task and not cls.redis_runner_task.done():
            logger.error('Already running')
            cls.redis_runner_task.cancel()
        cls.is_running = False
        cls.redis_runner_task = async_loop.create_task(cls.run_redis_listener())

    @classmethod
    @asyncio.coroutine
    def append(cls, path, ws):

        logger.info("add subscriber to {}".format(path))
        subscriber = Subscriber(path, ws)
        if path not in cls.connections:
            cls.connections[path] = []
            if not cls.subscriber:
                cls.start_redis_listener()
                # yield from cls.get_redis_subscriber()
            else:
                yield from cls.subscriber.unsubscribe(cls.subscriptions)
                cls.subscriptions = list(cls.connections.keys())
                # yield from cls.get_redis_subscriber()
                yield from cls.subscriber.subscribe(cls.subscriptions)
                logger.info("Resubscribe: Listening redis channels: {}".format(cls.subscriptions))

        cls.connections[path].append(subscriber)
        return subscriber

    @classmethod
    @asyncio.coroutine
    def get_redis_subscriber(cls):
        if cls.redis_connection and cls.subscriber:
            logger.debug("()()\t return existing subscriber")
            return cls.subscriber

        if not cls.redis_connection:
            params = get_params_from_url(redis_url)
            cls.redis_connection = yield from asyncio_redis.Connection.create(**params)
            logger.debug("****\tconnected to the redis")

        cls.subscriber = yield from cls.redis_connection.start_subscribe()

        yield from cls.subscriber.subscribe(cls.subscriptions)
        logger.info("Created subscriber: Listening redis channels: {}".format(cls.subscriptions))
        return cls.subscriber
