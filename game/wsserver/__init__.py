from asyncio_redis.exceptions import NotConnectedError
from django.core.cache import cache
from django.utils import timezone
from game.models import Gamer, Lobby
import asyncio
import websockets
import websockets.exceptions
from django.conf import settings
from .utils import get_params_from_url
# from django.core.cache import cache
import asyncio_redis
import logging
import json
from .subscriber import Subscriber
__author__ = 'vvf'


redis_url = settings.CACHES['default']['LOCATION']

async_loop = asyncio.get_event_loop()
logger = logging.getLogger(__name__)
redis_connection = None

@asyncio.coroutine
def get_redis_connection():
    global redis_connection
    if redis_connection:
        return redis_connection
    params = get_params_from_url(redis_url)
    redis_connection = yield from asyncio_redis.Pool.create(**params)
    return redis_connection

@asyncio.coroutine
def redis_publish(channel, message):
    global redis_connection
    redis_for_publish = yield from get_redis_connection()
    try:
        yield from redis_for_publish.publish(channel, message)
    except NotConnectedError:
        global redis_connection
        redis_connection = None
        yield from redis_publish(channel, message)

returnees = {}

@asyncio.coroutine
def alert_user_left_lobby(gamer):
    global returnees
    del returnees[gamer.id]
    gamer.join_time = None
    if not gamer.left_time and not gamer.lobby.start_time is None:
        gamer.left_time = timezone.now()
    gamer.save()
    lobby_cur_cnt=gamer.lobby.gamer_set.filter(join_time__isnull=False).count()
    logger.debug("<<< Gamer\"{}\" left lobby \"{}\" (del from lobby/{}, update in list cur_cnt={})".format(
        gamer.user.username,
        gamer.lobby.name,
        gamer.lobby_id,
        lobby_cur_cnt
    ))
    yield from redis_publish('/ws/lobby/{}'.format(gamer.lobby_id), json.dumps(dict(
        action='del',
        uid=gamer.user_id,
    )))
    yield from redis_publish('/ws/list', json.dumps(dict(
        action='update',
        id=gamer.lobby_id,
        cur_cnt=lobby_cur_cnt,
        status=gamer.lobby.get_status(),
    )))


@asyncio.coroutine
def dispatcher(ws, path):
    #depending on path just add to needed queue and loop forever (ignoring messages)
    logger.info('Connected to "{}"'.format(path))

    # find user in the game
    user_id = None
    lobby_id = None
    if path.startswith('/ws/lobby'):
        apath = path.split('/')
        user_id = apath.pop()
        lobby_id = apath[-1]
        path = '/'.join(apath)

    subscr = yield from Subscriber.append(path, ws)

    gamer = None
    if user_id and lobby_id:
        gamer = Gamer.objects.filter(user_id=user_id, lobby_id=lobby_id)\
            .select_related('user', 'lobby')\
            .first()
        if gamer and gamer.left_time:
            gamer.left_time = None
            if not gamer.join_time:
                gamer.join_time = timezone.now()
            gamer.save()
        if gamer and gamer.id in returnees:
            logger.debug("Cancel leaving {} from {}".format(
                gamer.user.username,
                gamer.lobby.name
            ))
            returnees[gamer.id].cancel()  # cancel his leaving
    while True:
        try:
            message = yield from ws.recv()
        except websockets.exceptions.ConnectionClosed:
            subscr.state = 'done'
            logger.debug("Websocket closed")
            if gamer:
                def _closure():
                    returnees[gamer.id] = async_loop.create_task(alert_user_left_lobby(gamer))
                async_loop.call_later(3, _closure)
            message = None
        if message is None:
            break
        # TODO: check is message allowed to relay
        msg_data = json.loads(message)
        if msg_data['action'] == 'chat':
            yield from redis_connection.publish(path, message)
        if msg_data['action'] == 'start' and gamer and gamer.is_creator:
    #   если после этого количество игроков достигло предела, то
            if gamer.lobby.gamer_set.count() >= gamer.lobby.gamers_count:
                gamer.lobby.start_time = timezone.now()
                gamer.lobby.save(update_fields=('start_time',))
        #       записать время начала игры в лобби
                # А вот если бы использовались очереди RabbitMQ,
                # то тут можно было бы поставить в очередь задачу "закрыть игру" через некоторое время
                async_loop.call_later(60, lambda: async_loop.create_task(finish_game_after_minute(gamer.lobby_id)))

                logger.debug("started timer to finish game")
        #       послать обновление статуса лобби,
            yield from redis_publish(path, message)

@asyncio.coroutine
def finish_game_after_minute(lobby_id):
    lobby = Lobby.objects.filter(id=lobby_id).first()
    if lobby and lobby.end_time is None:
        logger.debug("Finish game in {}".format(lobby.name))
        yield from redis_publish('/ws/lobby/{}'.format(lobby.id), json.dumps(dict(
            action='stop',
            id=lobby.id
        )))
    #       записать время окончания игры в лобби
        lobby.end_time = timezone.now()
        lobby.gamer_set.filter(left_time__isnull=True)\
            .update(left_time=lobby.end_time)
        lobby.save(update_fields=('end_time',))
        yield from redis_publish('/ws/list', json.dumps(dict(
            action='update',
            id=lobby_id,
            status=lobby.get_status(),
        )))

@asyncio.coroutine
def dummy_pinger():
    redis_connection = yield from get_redis_connection()
    while True:
        yield from asyncio.sleep(10)
        yield from redis_publish('dummy', 'ping '+timezone.now().strftime('%H:%M:%S'))

test_data = {}
@asyncio.coroutine
def test():
    global test_data
    connection = yield from asyncio_redis.Connection.create(host='localhost', port=6379)

    # Create subscriber.
    subscriber = yield from connection.start_subscribe()
    test_data['subscriber'] = subscriber
    test_data['channels'] = ['dummy', 'sss']

    # Subscribe to channel.
    yield from subscriber.subscribe(test_data['channels'])

    # Inside a while loop, wait for incoming events.
    while True:
        reply = yield from subscriber.next_published()
        print('Received: ', repr(reply.value), 'on channel', reply.channel)

    # When finished, close the connection.
    connection.close()

@asyncio.coroutine
def stoptest():
    global test_data
    for ch in 'some one /ws/list /ws/lobby/38 done'.split(' '):
        yield from asyncio.sleep(10)

        yield from test_data['subscriber'].unsubscribe(test_data['channels'])
        # Subscribe to channel.
        test_data['channels'] += [ch]
        print("resubscribe to {}".format(test_data['channels']))
        yield from test_data['subscriber'].subscribe(test_data['channels'])

def run_server(host='localhost', port=8765):
    global test_task
    logger.info('Start websockets server on {}:{}'.format(host, port))

    # test_task = async_loop.create_task(test())
    # asyncio.async(async_loop.create_task(stoptest()))
    # async_loop.run_until_complete(test())

    start_server = websockets.serve(dispatcher, host, port)
    async_loop.run_until_complete(start_server)
    # asyncio.async(async_loop.create_task(dummy_pinger()))
    Subscriber.start_redis_listener()
    async_loop.run_forever()
